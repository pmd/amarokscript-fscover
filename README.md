README
================================

amarokscript-fscover is an amarok script that implements a fullscreen cover.
Although, you probably knew that. Using nothing but the Qt javascript
bindings, , this 70-line-ish script creates a menu entry, takes the cover
image amarok is using, creates a reflection using QPainter, fetches the
current track's tags, and then slaps it all together into one fullscreen
widget with a black background. To close the full-screen mode, either press
your default window-closing keybind (alt+f4/etc), or press the invisible 50x50
button in the top-left corner.

### `Note`
This script wont support embedded album art for two reasons. 
The first, is that embedded art is bad. It inflates files, demands 
support for another (unnecessary) feature, and sets a bad precedent in 
regards to scan quality. The second, is that Amarok.Engine.currentTrack().imageUrl does not
return as a filesystem path when the art is embedded, and hunting through the
amarok database is far more work than should be neccessary for just showing
an image.

### `Screenshots`

![Full-screen covers](http://sbin.co.uk/screenshots/amarok.png)
