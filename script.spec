[Desktop Entry]
Icon=view-fullscreen
Type=script
ServiceTypes=KPluginInfo
Name=Fullscreen Cover Mode

Comment=A script that adds a fullscreen cover mode to amarok.

X-KDE-PluginInfo-Author=Patrick McDonough
X-KDE-PluginInfo-Email=patrickmcdonough@fsfe.org
X-KDE-PluginInfo-Name=Fullscreen Cover
X-KDE-PluginInfo-Version=0.01
X-KDE-PluginInfo-Category=Scriptable Service
X-KDE-PluginInfo-Depends=Amarok2.0
X-KDE-PluginInfo-License=GPLv3
X-KDE-PluginInfo-EnabledByDefault=false
