Importer.loadQtBinding( "qt.core" );
Importer.loadQtBinding( "qt.gui" );

// QWidget as a host for our image and text
var cover_widget = new QWidget();
// Make sure if you change the background colour, you change QPainter's too.
cover_widget.setStyleSheet( 
  "background-color: black; color: white; font-size: 20px;" );

// A quit button, since QtScript has limitations on what is possible.
var fs_button = new QPushButton( "", cover_widget );
fs_button.flat = true;
fs_button.setGeometry( 0, 0, 50, 50 );
fs_button.clicked.connect(switchFS);

// We store the image in a label; SetPixmap() will do the lifting.
var lb_fs_art = new QLabel();
lb_fs_art.alignment = ( Qt.AlignCenter );

// Tag labels
var lb_tag1 = new QLabel();
lb_tag1.alignment = ( Qt.AlignHCenter );
var lb_tag2 = new QLabel();
lb_tag2.alignment = ( Qt.AlignHCenter );

// Place all the labels inside a vertical box, for automatic vertical placement.
var vbox_cover = new QVBoxLayout( cover_widget );
vbox_cover.addWidget( lb_fs_art, 1, Qt.AlignCenter );
vbox_cover.addWidget( lb_tag1, 0, Qt.AlignBottom );
vbox_cover.addWidget( lb_tag2, 0, Qt.AlignBottom );

// Amarok-specific code to add a menu entry.
Amarok.Window.addToolsSeparator();
Amarok.Window.addToolsMenu( 
  "FULL_SCREEN_COVER", "Fullscreen Cover Mode", "view-fullscreen" );
Amarok.Window.ToolsMenu.FULL_SCREEN_COVER['triggered()'].connect( switchFS );

Amarok.Engine.trackChanged.connect( newTrack );

function newTrack()
{
/*  Available track metadata from Amarok.Engine.currenttrack()

    samplerate  -> current track's samplerate
    bitrate     -> current track's bitrate
    score       -> current track's score (Amarok-specific?)
    rating      -> current track's rating (Stored in tag?)
    length      -> current track's length
    tracknumber -> current track's number
    discnumber  -> current track's disc number
    playcount   -> current track's play count
    bpm         -> current track's bpm

    title       -> current track's title
    artist      -> current track's artist
    album       -> current track's album
    composer    -> current track's composer
    genre       -> current track's genre
    year        -> current track's year
    comment     -> current track's comment

    path        -> current track's folder
    imageUrl    -> current track's cover.
    url         -> current track's path
    bpm         -> current track's bpm
*/


// Fetch the current track's cover url.
    var art_url = Amarok.Engine.currentTrack().imageUrl;
// Substring it to get rid of "file://"
    art_url = art_url.substr( 7 );
// Get the tags for the labels - use anything from the table above.
    lb_tag1.setText( "" + Amarok.Engine.currentTrack().title 
                     + "<i> by </i> " + Amarok.Engine.currentTrack().artist );
    lb_tag2.setText( "<i>from</i> <b>" + Amarok.Engine.currentTrack().album 
                     + "</b><br />(" + Amarok.Engine.currentTrack().year 
                     + ")" );

// Copy over the cover into a QImage object to work on.
    var qimg_cover = new QImage( art_url );
// If there isn't a cover, use a local image.
    if (!art_url) { qimg_cover.load( (Amarok.Info.scriptPath() 
                                      + "/nocover.png"), 'PNG'); }

// Scale the image. Make a mirror copy of it, crop the reflection.
// We only need the top 150px or so.
    qimg_cover = qimg_cover.scaledToHeight( 500, Qt.SmoothTransformation );
    var reflection = new QImage( qimg_cover.mirrored() );
    reflection = reflection.copy( 0, 0, qimg_cover.width(), 150 );

// Make a black gradient to apply over the reflection.
// Try settings more points to change the way the reflection looks.
    var start = new QPoint( 0, 0 );
    var end = new QPoint( 0, 200 );
    var qcol_1 = new QColor( 0, 0, 0, 150 );
    var qcol_2 = new QColor( 0, 0, 0, 255 );
    var gradient = new QLinearGradient( start, end );
    gradient.setColorAt( 0, qcol_1 );
    gradient.setColorAt( 0.5, qcol_2 );
    var grad_brush = new QBrush( gradient );

// Painter object to apply the gradient (which we use as a brush).
    var painter = new QPainter();
    painter.setCompositionMode( QPainter.CompositionMode_Clear );
    painter.begin( reflection );
    painter.fillRect( reflection.rect(), grad_brush );
    painter.end();

// Create a new image, the length of the original + reflection.
    var final_cover = new QImage( qimg_cover.width(), 
                                ( qimg_cover.height() + 150 ), 
                                  QImage.Format_RGB32 );
    final_cover.fill(0);

// Draw the original image, then draw the reflection 1 pixel below it.
    var f_paint = new QPainter();
    f_paint.begin( final_cover );
    f_paint.drawImage( 0, 0, qimg_cover );
    f_paint.drawImage( 0, ( qimg_cover.height() + 1 ), reflection );
    f_paint.end();

// Set the label to the new image.
    lb_fs_art.setPixmap( QPixmap.fromImage( final_cover ) );
}

function switchFS()
{
    if (cover_widget.fullScreen)
    {
        cover_widget.hide();
    }
    else
    {
        cover_widget.showFullScreen();
    }
}
